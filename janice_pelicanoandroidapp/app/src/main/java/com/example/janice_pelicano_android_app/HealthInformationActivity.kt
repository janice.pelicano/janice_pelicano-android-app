package com.example.janice_pelicano_android_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.janice_pelicano_android_app.databinding.ActivityHealthInformationBinding

class HealthInformationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityHealthInformationBinding

    private var listTasks: MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHealthInformationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init(){
        val fullName = intent.extras?.getString("fullName")
        val phone = intent.extras?.getString("phone")
        val city = intent.extras?.getString("city")
        binding.healthInfoBtn.setOnClickListener{
            val intent = Intent(this, ConfirmationActivity::class.java)
            intent.putExtra("fullName",fullName.toString())
            intent.putExtra("phone",phone.toString())
            intent.putExtra("city",city.toString())
            intent.putExtra("ans1",binding.ans1.text.toString())
            intent.putExtra("ans2",binding.ans2.text.toString())
            startActivity(intent)
        }
    }

}