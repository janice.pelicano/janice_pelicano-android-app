package com.example.janice_pelicano_android_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.janice_pelicano_android_app.databinding.ActivityConfirmationBinding
import com.google.android.material.textfield.TextInputEditText

class ConfirmationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityConfirmationBinding

    private var listTasks: MutableList<String> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityConfirmationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()

        val fullName = intent.extras?.getString("fullName")
        val phone = intent.extras?.getString("phone")

        val viewFullName = findViewById<View>(R.id.view_full_name) as TextInputEditText
        viewFullName.setText(fullName.toString())

        val viewMobile = findViewById<View>(R.id.view_phone) as TextInputEditText
        viewMobile.setText(phone.toString())
    }

    private fun init() {
        setUpRecyclerView()
        binding.confirmBtn.setOnClickListener {
            val intent = Intent(this,SuccessActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setUpRecyclerView()
    {
        val fullName = intent.extras?.getString("fullName")
        val phone = intent.extras?.getString("phone")
        val city = intent.extras?.getString("city")
        val ans1 = intent.extras?.getString("ans1")
        val ans2 = intent.extras?.getString("ans2")

        listTasks.add(fullName.toString())
        listTasks.add(phone.toString())
        listTasks.add(city.toString())
        listTasks.add(ans1.toString())
        listTasks.add(ans2.toString())

        binding.recyclerView.apply {
            adapter = RecyclerViewAdapter(list = listTasks)
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        }
    }

}