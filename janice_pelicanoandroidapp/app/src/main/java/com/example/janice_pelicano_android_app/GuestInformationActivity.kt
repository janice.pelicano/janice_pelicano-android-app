package com.example.janice_pelicano_android_app

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.janice_pelicano_android_app.databinding.ActivityGuestInformationBinding


class GuestInformationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGuestInformationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGuestInformationBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()

    }

    private fun init(){
        binding.nextButton.setOnClickListener{
            val intent = Intent(this, HealthInformationActivity::class.java)
            intent.putExtra("fullName",binding.fullNameEditText.text.toString())
            intent.putExtra("phone",binding.phoneEditText.text.toString())
            intent.putExtra("city",binding.cityEditText.text.toString())
            startActivity(intent)
        }
    }
}