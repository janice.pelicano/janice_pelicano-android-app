package com.example.janice_pelicano_android_app

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class SuccessActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_success)
    }
}